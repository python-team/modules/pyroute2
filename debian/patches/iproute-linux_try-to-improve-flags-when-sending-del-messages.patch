Description: iproute/linux: try to improve flags when sending del messages 
 Note from package maintainer.
 .
 This is a backport of this patch:
 https://github.com/svinota/pyroute2/commit/1eb08312de30a083bcfddfaa9c1d5e124b6368df#diff-e412e6093798df68279c3c7a0887cd91baef4d17a58c2bcfe10263f1e1c29ef0R803
 to version 0.5.14 of pyroute2. It is needed after the kernel upgrade
 that contains this commit:
 https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=a6cec0bcd34264be8887791594be793b3f12719f
 for a yet-to-be identified kernel version.
 .
 Flags are not the same between NEW and DELETE request. In file netlink.h
 we can see:
 .
 /* Modifiers to NEW request */
 #define NLM_F_REPLACE   0x100   /* Override existing        */
 #define NLM_F_EXCL      0x200   /* Do not touch, if it exists   */
 #define NLM_F_CREATE    0x400   /* Create, if it does not exist */
 #define NLM_F_APPEND    0x800   /* Add to end of list       */
 .
 /* Modifiers to DELETE request */
 #define NLM_F_NONREC    0x100   /* Do not delete recursively    */
 #define NLM_F_BULK      0x200   /* Delete multiple objects  */
 .
 So NLM_F_BULK is the same value than NLM_F_EXCL. NLM_F_BULK has been
 added in this kernel commit:
 .
 https://git.kernel.org/pub/scm/linux/kernel/git/netdev/net-next.git/commit/?id=a6cec0bcd34264be8887791594be793b3f12719f
 .
 When this flag is set, pyroute now get a lot of error everywhere when
 trying to delete objects.
 .
 Note that kernel commit is not yet mainline, and still in developpment
 branch. But it can have a huge impact on pyroute2 library
 .
Author: Florent Fourcot <florent.fourcot@wifirst.fr>
Origin: upstream, https://github.com/svinota/pyroute2/commit/1eb08312de30a083bcfddfaa9c1d5e124b6368df
Last-Update: 2024-06-07

--- pyroute2-0.5.14.orig/pyroute2/iproute/linux.py
+++ pyroute2-0.5.14/pyroute2/iproute/linux.py
@@ -561,7 +561,7 @@ class RTNL_API(object):
             # flush all addresses with IFA_LABEL='eth0':
             ipr.flush_addr(label='eth0')
         '''
-        flags = NLM_F_CREATE | NLM_F_EXCL | NLM_F_REQUEST
+        flags = NLM_F_CREATE | NLM_F_REQUEST
         ret = []
         for addr in self.get_addr(*argv, **kwarg):
             self.put(addr, msg_type=RTM_DELADDR, msg_flags=flags)
@@ -582,7 +582,7 @@ class RTNL_API(object):
             # flush all IPv6 rules that point to table 250:
             ipr.flush_rules(family=socket.AF_INET6, table=250)
         '''
-        flags = NLM_F_CREATE | NLM_F_EXCL | NLM_F_REQUEST
+        flags = NLM_F_CREATE | NLM_F_REQUEST
         ret = []
         for rule in self.get_rules(*argv, **kwarg):
             self.put(rule, msg_type=RTM_DELRULE, msg_flags=flags)
@@ -936,9 +936,9 @@ class RTNL_API(object):
                     'set': (RTM_NEWNEIGH, flags_replace),
                     'replace': (RTM_NEWNEIGH, flags_replace),
                     'change': (RTM_NEWNEIGH, flags_change),
-                    'del': (RTM_DELNEIGH, flags_make),
-                    'remove': (RTM_DELNEIGH, flags_make),
-                    'delete': (RTM_DELNEIGH, flags_make),
+                    'del': (RTM_DELNEIGH, flags_base),
+                    'remove': (RTM_DELNEIGH, flags_base),
+                    'delete': (RTM_DELNEIGH, flags_base),
                     'dump': (RTM_GETNEIGH, flags_dump),
                     'get': (RTM_GETNEIGH, flags_base),
                     'append': (RTM_NEWNEIGH, flags_append)}
@@ -1305,9 +1305,9 @@ class RTNL_API(object):
         commands = {'set': (RTM_NEWLINK, flags_req),
                     'update': (RTM_SETLINK, flags_create),
                     'add': (RTM_NEWLINK, flags_create),
-                    'del': (RTM_DELLINK, flags_create),
-                    'remove': (RTM_DELLINK, flags_create),
-                    'delete': (RTM_DELLINK, flags_create),
+                    'del': (RTM_DELLINK, flags_req),
+                    'remove': (RTM_DELLINK, flags_req),
+                    'delete': (RTM_DELLINK, flags_req),
                     'dump': (RTM_GETLINK, flags_dump),
                     'get': (RTM_GETLINK, NLM_F_REQUEST)}
 
@@ -1423,9 +1423,9 @@ class RTNL_API(object):
         flags_create = flags_base | NLM_F_CREATE | NLM_F_EXCL
         flags_replace = flags_base | NLM_F_REPLACE | NLM_F_CREATE
         commands = {'add': (RTM_NEWADDR, flags_create),
-                    'del': (RTM_DELADDR, flags_create),
-                    'remove': (RTM_DELADDR, flags_create),
-                    'delete': (RTM_DELADDR, flags_create),
+                    'del': (RTM_DELADDR, flags_base),
+                    'remove': (RTM_DELADDR, flags_base),
+                    'delete': (RTM_DELADDR, flags_base),
                     'replace': (RTM_NEWADDR, flags_replace),
                     'dump': (RTM_GETADDR, flags_dump)}
         (command, flags) = commands.get(command, command)
@@ -1566,17 +1566,17 @@ class RTNL_API(object):
         flags_replace = flags_change | NLM_F_CREATE
 
         commands = {'add': (RTM_NEWQDISC, flags_make),
-                    'del': (RTM_DELQDISC, flags_make),
-                    'remove': (RTM_DELQDISC, flags_make),
-                    'delete': (RTM_DELQDISC, flags_make),
+                    'del': (RTM_DELQDISC, flags_base),
+                    'remove': (RTM_DELQDISC, flags_base),
+                    'delete': (RTM_DELQDISC, flags_base),
                     'change': (RTM_NEWQDISC, flags_change),
                     'replace': (RTM_NEWQDISC, flags_replace),
                     'add-class': (RTM_NEWTCLASS, flags_make),
-                    'del-class': (RTM_DELTCLASS, flags_make),
+                    'del-class': (RTM_DELTCLASS, flags_base),
                     'change-class': (RTM_NEWTCLASS, flags_change),
                     'replace-class': (RTM_NEWTCLASS, flags_replace),
                     'add-filter': (RTM_NEWTFILTER, flags_make),
-                    'del-filter': (RTM_DELTFILTER, flags_make),
+                    'del-filter': (RTM_DELTFILTER, flags_base),
                     'change-filter': (RTM_NEWTFILTER, flags_change),
                     'replace-filter': (RTM_NEWTFILTER, flags_replace)}
         if isinstance(command, int):
@@ -1856,9 +1856,9 @@ class RTNL_API(object):
                     'replace': (RTM_NEWROUTE, flags_replace),
                     'change': (RTM_NEWROUTE, flags_change),
                     'append': (RTM_NEWROUTE, flags_append),
-                    'del': (RTM_DELROUTE, flags_make),
-                    'remove': (RTM_DELROUTE, flags_make),
-                    'delete': (RTM_DELROUTE, flags_make),
+                    'del': (RTM_DELROUTE, flags_base),
+                    'remove': (RTM_DELROUTE, flags_base),
+                    'delete': (RTM_DELROUTE, flags_base),
                     'get': (RTM_GETROUTE, NLM_F_REQUEST),
                     'show': (RTM_GETROUTE, flags_dump),
                     'dump': (RTM_GETROUTE, flags_dump)}
@@ -1993,9 +1993,9 @@ class RTNL_API(object):
         flags_dump = NLM_F_REQUEST | NLM_F_ROOT | NLM_F_ATOMIC
 
         commands = {'add': (RTM_NEWRULE, flags_make),
-                    'del': (RTM_DELRULE, flags_make),
-                    'remove': (RTM_DELRULE, flags_make),
-                    'delete': (RTM_DELRULE, flags_make),
+                    'del': (RTM_DELRULE, flags_base),
+                    'remove': (RTM_DELRULE, flags_base),
+                    'delete': (RTM_DELRULE, flags_base),
                     'dump': (RTM_GETRULE, flags_dump)}
         if isinstance(command, int):
             command = (command, flags_make)
